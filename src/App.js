import React, { Component } from 'react';
import './App.css';
import { Header, Navigation, Drawer, Content, Layout} from 'react-mdl';
import Main from './components/main';
import { Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="demo-big-content">
    <Layout>
        <Header title="Portfolio" scroll>
            <Navigation>
                <Link to ="/aboutme">About Me</Link>
                <Link to ="/resume">Resume</Link>
                <Link to ="/projects">Projects</Link>
                <Link to ="/contacts">Contacts</Link>
            </Navigation>
        </Header>
        <Drawer title="Portfolio">
            <Navigation>
              <Link to ="/aboutme">About Me</Link>
              <Link to ="/resume">Resume</Link>
              <Link to ="/projects">Projects</Link>
              <Link to ="/contacts">Contacts</Link>
            </Navigation>
        </Drawer>
        <Content>{/*here's where the body lies*/}
            <div className="page-content" />
            <Main/>{/*to render the 'main' component*/}
        </Content>
    </Layout>
</div>
    );
  }
}

export default App;
